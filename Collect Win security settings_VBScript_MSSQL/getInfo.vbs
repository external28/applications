'��������������������������
'Info to Base 
'��������������������������

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Option statements																	'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Definitions																		'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public groupListD
Public strUsersArray()
Public numUsersArraySize
numUsersArraySize=25
ReDim strUsersArray(numUsersArraySize)
Public strGetOsNameResult

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function Main																		'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	Dim strComputerName, strComputerIP, strComputerIPType
	Dim objLocalAdminGroup
	Dim objLocalPUGroup
	Dim strLocalAdminGroupName
	Dim strLocalPUGroupName
	Dim strLocalDateTime
	Dim strLocalAdminGroupMembers
	Dim strLocalPUGroupMembers
	Dim strLoginName, strPassChangeDate
	Const APPENDING = 8
	Dim strOSName
        Dim strUSBStatus
        Dim strFDDStatus
        Dim strCDDVDStatus
        Dim strIsAdmin
        Dim strAVData 
	
	strComputerName= GetComputerName
	strLoginName= GetLoginName
	GPMember
	strOSName = GetOSName
	'strUSBStatus = ReadWPUSBStor
	'strAVData = GetAVDate 
	strPassChangeDate= GetPassChangeDate (strComputerName)
	strComputerIP= GetComputerIP(strComputerIPType)

	If strComputerName<>"" Then
		strLocalDateTime= GetComputerDateTime ()
		GetLocalAdminUsers (".")
		If strUsersArray(0)>=0 Then
			strLocalAdminGroupMembers= PrintRecords()
		End If
		GetLocalPowerUsers (".")
		If strUsersArray(0)>=0 Then
			strLocalPUGroupMembers= PrintRecords()
		End If
        
	strIsAdmin = CheckIsAdmin
	
        strUSBStatus = "disabled"
        strFDDStatus = "disabled"
        strCDDVDStatus = "disabled"

	If CBool(groupListD.Exists("Group_USB_read_only")) then
		strUSBStatus = "read-only"
	End If
	If CBool(groupListD.Exists("Group_USB_read_write")) then
		strUSBStatus = "read-write"
	End If

	If CBool(groupListD.Exists("Group_Floppy_read_only")) then
		strFDDStatus = "read-only"
	End If
	If CBool(groupListD.Exists("Group_Floppy_read_write")) then
		strFDDStatus = "read-write"
	End If

	If CBool(groupListD.Exists("Group_CD_DVD_read_only")) then
		strCDDVDStatus = "read-only"
	End If
	If CBool(groupListD.Exists("Group_CD_DVD_read_write")) then
		strCDDVDStatus = "read-write"
	End If


SaveToSQLDataBase strComputerName, strComputerIP, strComputerIPType, strLocalAdminGroupMembers, strLocalPUGroupMembers, strLocalDateTime, strLoginName, strPassChangeDate, strOSName, strUSBStatus, strFDDStatus, strCDDVDStatus, strIsAdmin,strAVData
	End If


' *****************************************************
Function EnvString(variable)
    Dim objShell
    set objShell = WScript.CreateObject( "WScript.Shell" )
    variable = "%" & variable & "%"
    EnvString = objShell.ExpandEnvironmentStrings(variable)
End Function
' *****************************************************

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GPMember ��������� �������� �������� ����� AD									'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function GPMember
    Dim ADSPath
    Dim userPath
    Dim listGroup   
    If IsEmpty(groupListD) then
        Set groupListD = CreateObject("Scripting.Dictionary")
        groupListD.CompareMode = 1
        ADSPath = EnvString("userdomain") & "/" & EnvString("username")
        Set userPath = GetObject("WinNT://" & ADSPath & ",user")
        For Each listGroup in userPath.Groups
            groupListD.Add listGroup.Name, "-"
        Next
    End if
End Function


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GetLocalAdminUsers												'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 
Function GetLocalAdminUsers(strComputerName)
	On Error Resume Next
	
	Dim strUserName
	Dim objMemeber
	Dim numCount
	Dim bGroup
	
	'Dim strUsersArray(25)'First element is count of items in array, Second is a GroupName if first element <0, then error
	
	strLocalAdminGroupName="Administrators"
	bGroup=False
	
	Err.Clear
	Set objLocalAdminGroup=GetObject ("WinNT://"&strComputerName&"/"&strLocalAdminGroupName&",group")
	
	If Err.Number <> 0 Then
		'WScript.Echo "������ on GetObject Group "&"# 0x"&(Hex(Err.Number))&" "&Err.Description
		Err.Clear
	
		strLocalAdminGroupName="��������������"
		Set objLocalAdminGroup=GetObject ("WinNT://"&strComputerName&"/"&strLocalAdminGroupName&",group")
		If Err.Number <> 0 Then
			'WScript.Echo "������ on GetObject Group "&"# 0x"&(Hex(Err.Number))&" "&Err.Description
			Err.Clear
			bGroup=false
		Else
			bGroup=true
		End If
	Else
			bGroup=true
	End if
	
	numCount=-1
	
	If bGroup=True then
	numCount=0
	'WScript.Echo "������  "&strComputerName&"/"&strLocalAdminGroupName&vbCrLf
	strUsersArray(0)=0
	strUsersArray(1)=strLocalAdminGroupName
	
		For Each objMemeber In objLocalAdminGroup.Members
			'strUserName=objMemeber.Name
			strUserName=objMemeber.AdsPath
			strUserName=Replace(strUserName,"WinNT://","",1,1)
			strUserName=Replace(strUserName,"/","\",1,1)
			If Not Left(strUserName,3) = "RS\" Then
                           strUserName = Mid(strUserName, InStr(strUserName, "\") + 1, Len(strUserName))
			End if
			'WScript.Echo vbCrLf&"User: "& strUserName
			strUsersArray(numCount+2)=strUserName
			numCount=numCount+1
		Next
	'WScript.Echo "����� �������������: "&numCount&vbCrLf
	End if
	
	strUsersArray(0)=numCount
	'GetLocalAdminUsers=strUsersArray
	GetLocalAdminUsers=bGroup

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetLocalAdminUsers", ""
   Err.Clear
End If
On Error Goto 0
End Function


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GetLocalPowerUsers							    '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 
Function GetLocalPowerUsers(strComputerName)
	On Error Resume Next
	
	Dim strUserName
	Dim objMemeber
	Dim numCount
	Dim bGroup
	
	
	strLocalPUGroupName="Power users"
	bGroup=False
	
	Err.Clear
	Set objLocalPUGroup=GetObject ("WinNT://"&strComputerName&"/"&strLocalPUGroupName&",group")
	
	If Err.Number <> 0 Then
		'WScript.Echo "������ on GetObject Group "&"# 0x"&(Hex(Err.Number))&" "&Err.Description
		Err.Clear
	
		strLocalPUGroupName="������� ������������"
		Set objLocalPUGroup=GetObject ("WinNT://"&strComputerName&"/"&strLocalPUGroupName&",group")
		If Err.Number <> 0 Then
			Err.Clear
			bGroup=false
		Else
			bGroup=true
		End If
	Else
			bGroup=true
	End if
	
	numCount=-1
	
	If bGroup=True then
	numCount=0
	'WScript.Echo "������  "&strComputerName&"/"&strLocalPUGroupName&vbCrLf
	strUsersArray(0)=0
	strUsersArray(1)=strLocalPUGroupName
	
		For Each objMemeber In objLocalPUGroup.Members
			'strUserName=objMemeber.Name
			strUserName=objMemeber.AdsPath
			strUserName=Replace(strUserName,"WinNT://","",1,1)
			strUserName=Replace(strUserName,"/","\",1,1)
			If Not Left(strUserName,3) = "RS\" Then
                           strUserName = Mid(strUserName, InStr(strUserName, "\") + 1, Len(strUserName))
			End if
			'WScript.Echo vbCrLf&"User: "& strUserName
			strUsersArray(numCount+2)=strUserName
			numCount=numCount+1
		Next
	'WScript.Echo "����� �������������: "&numCount&vbCrLf
	End if
	
	strUsersArray(0)=numCount

	GetLocalPowerUsers=bGroup

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetLocalPowerUsers", ""
   Err.Clear
End If
On Error Goto 0
End Function


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function PrintRecords																	'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 
Function PrintRecords
	On Error Resume Next
	Dim StrResult
	Dim i, NumOfUsers
	NumOfUsers=strUsersArray(0)
	i=2
	While (i<numUsersArraySize) And (i<=(NumOfUsers+1))
		'WScript.Echo "User: "& strUsersArray(i)
		StrResult=StrResult&""""&strUsersArray(i)&""""
		If (i<(NumOfUsers+1)) Then
			StrResult=StrResult&","
		End If
		i=i+1
	Wend
	'WScript.Echo "����� �������������: "&NumOfUsers&vbCrLf
	
	PrintRecords=StrResult

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description , "PrintRecords", ""
   Err.Clear
End If
On Error Goto 0

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GetComputerName															'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function GetComputerName
On Error Resume Next


	Dim WshNetwork
	Set WshNetwork=WScript.CreateObject("WScript.Network")
	GetComputerName=WshNetwork.ComputerName

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetComputerName", ""
   Err.Clear
End If
On Error Goto 0
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GetComputerIP																'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function GetComputerIP (ByRef strComputerIPType)
On Error Resume Next

	Dim strComputer
	Dim strIPType
	Dim strComputerIPAddress
	strComputer = "." 
	Dim objWMIService
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	
	Dim IPConfigSet, IPConfig
	Set IPConfigSet = objWMIService.ExecQuery ("Select * from Win32_NetworkAdapterConfiguration where IPEnabled=TRUE") 
	  
	Dim i, i1
'	i1=1
	For Each IPConfig in IPConfigSet 
		If Not IsNull(IPConfig.IPAddress) Then
			For i=LBound(IPConfig.IPAddress) to UBound(IPConfig.IPAddress) 
					'WScript.Echo IPConfig.IPAddress(i)&" - "&IPConfig.Description(i)&" i1="&i1&" - "&strIPType&vbCrLf
'				If i1<=1 Then
                                        If InStr(strComputerIPAddress, IPConfig.IPAddress(i)) = 0 AND IPConfig.IPAddress(i) <> "0.0.0.0" Then
                                        	If (IPConfig.DHCPEnabled=True) Then
						strIPType="DHCP (Server "&IPConfig.DHCPServer&")"
					        Else
						strIPType="Static"
            					End If
	         				strComputerIPType= strComputerIPType & strIPType & " "
					   strComputerIPAddress = strComputerIPAddress & IPConfig.IPAddress(i) & " "
'					MsgBox "1:   " & strComputerIPAddress
'					MsgBox "2:   " & strComputerIPType
					End If
'					If (strComputerIPAddress="0.0.0.0") Or (strComputerIPAddress="") Then
'						i1=0
'					End If
'				End If
'			i1=i1+1
			Next 
		End If 
	Next 
GetComputerIP = strComputerIPAddress


If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetComputerIP", ""
   Err.Clear
End If
On Error Goto 0
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GetComputerDateTime														'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function GetComputerDateTime
On Error Resume Next

	Dim strComputerDateTime
	strComputerDateTime=""&Year(Date)&"-"&Month(Date)&"-"&Day(Date)&" "&Hour(Time)&":"&Minute(Time)
		
	GetComputerDateTime=strComputerDateTime

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetComputerDateTime",""
   Err.Clear
End If

On Error Goto 0
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function SaveToSQLDataBase															'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 
Function SaveToSQLDataBase (strComputerName, strComputerIP, strComputerIPType, strComputerLocalAdmins, strComputerLocalPU, strComputerDate, strLoginName, strPassChangeDate, strOSName, strUSBStatus, strFDDStatus, strCDDVDStatus, strIsAdmin, strAVData)
On Error Resume Next
	Dim objConn, objRecordset
	Dim ServerName, DSN, UID, PWD, ConnectString, ExecuteString
	
	'������ ��� �����������
	Set objConn = CreateObject("ADODB.Connection")
	
	'���������� ��������� ����������� � ���� ������
	ServerName = "" 		'��� ��� IP-����� �������
	DSN = "" 			'��� ���� ������
	UID = "" 			'����� ������������ SQL-�������
	PWD = "" 	'������ ������������ SQL-�������
	
	ConnectString = "Provider=SQLOLEDB;"&"Data Source="&ServerName&";Initial Catalog="&DSN&";UID="&UID&";PWD="&PWD
	objConn.ConnectionString = ConnectString
	objConn.ConnectionTimeOut = 15
	objConn.CommandTimeout = 30
	
	'������������ � ���� ������
	objConn.Open
	
	'��������� ������
'ExecuteString="EXEC SaveDataToCompAdmins2 '"&strComputerName&"', '"&strComputerIP&"', '"&strComputerIPType&"', '"&strComputerLocalAdmins&"', '"&strComputerLocalPU&"', '"&strComputerDate&"', '"&strLoginName&"', '"&strPassChangeDate&"', '"&strOSName&"', '"&strUSBStatus&"', '"&strFDDStatus&"', '"&strCDDVDStatus&"', '"&strIsAdmin&"', '"&strAVData&"'"
											
	'��������� ������
	Set objRecordset = objConn.Execute(ExecuteString)

        '��������� ������2
	ExecuteString="EXEC SaveDataToCompAdminsWOU2 '"&strComputerName&"', '"&strComputerIP&"', '"&strComputerIPType&"', '"&strComputerLocalAdmins&"', '"&strComputerLocalPU&"', '"&strComputerDate&"', '"&strLoginName&"', '"&strPassChangeDate&"', '"&strOSName&"', '"&strUSBStatus&"', '"&strFDDStatus&"', '"&strCDDVDStatus&"', '"&strIsAdmin&"', '"&strAVData&"'"
	'��������� ������2
	Set objRecordset = objConn.Execute(ExecuteString)

	'��������� ����������
	objConn.Close
	Set objConn = Nothing
	Set objRecordset = Nothing
	

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "SaveToSQLDataBase", ""
   Err.Clear
End If
On Error Goto 0

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GetLoginName																'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function GetLoginName
On Error Resume Next

	Dim WshNetwork
	Set WshNetwork=WScript.CreateObject("WScript.Network")
	GetLoginName= WshNetwork.UserDomain &"\"& WshNetwork.UserName
If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetLoginName", ""
   Err.Clear
End If
On Error Goto 0
End Function


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GetPassChangeDate															'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function GetPassChangeDate (strComputerName)
On Error Resume Next

Dim intPasswordAge
Dim dtmChangeDate
Dim objUser
Dim strAdminName
Dim strPLCD
Dim objWMIService
Dim colAccounts
Dim objAccount


Set objWMIService = GetObject("winmgmts:\\" & strComputerName & "\root\cimv2")
Set colAccounts = objWMIService.ExecQuery _
    ("Select Name, SID From Win32_UserAccount WHERE Domain = '" & strComputerName & "' ")
For Each objAccount in colAccounts
    If Left (objAccount.SID, 6) = "S-1-5-" and Right(objAccount.SID, 4) = "-500" Then
        strAdminName = objAccount.Name
	Exit For
    End If
Next
Set objUser = GetObject("WinNT://" & strComputerName & "/" & strAdminName)


intPasswordAge = objUser.PasswordAge
intPasswordAge = intPasswordAge * -1
dtmChangeDate = DateAdd("s", intPasswordAge, Now)
strPLCD= Year(dtmChangeDate)&"-"&Month(dtmChangeDate)&"-"&Day(dtmChangeDate)
GetPassChangeDate = strPLCD

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetPassChangeDate", ""
   Err.Clear
   End If
On Error Goto 0
End Function
'======================================


''''''''''''''''''''''''''''''''''''''
Function GetOSName
''''''''''''''''''''''''''''''''''''''
On Error Resume Next
Dim objWMIService
Dim colOSes
Dim objOS
GetOSName = ""

Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & "." & "\root\cimv2")
Set colOSes = objWMIService.ExecQuery("Select * from Win32_OperatingSystem")
For Each objOS in colOSes
    If objOS.Caption<>"" Then
	GetOSName = GetOSName & objOS.Caption & "  " & objOS.Version	
	strGetOsNameResult= GetOSName 
    Else
	GetOSName = GetOSName & "unknown"
	strGetOsNameResult =  GetOSName 
    End If

Next

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetOSName", ""
   Err.Clear
End If
On Error GoTo 0
End Function
'======================================

''''''''''''''''''''''''''''''''''''''
Function ReadWPUSBStor
''''''''''''''''''''''''''''''''''''''
On Error Resume Next
Dim objShell
Dim strWriteProtect, strUsbStor
Dim funcresult
Dim objWMI, objInstances

funcresult = 0

strWriteProtect = "HKLM\SYSTEM\CurrentControlSet\Control\StorageDevicePolicies\WriteProtect"
strUsbStor = "HKLM\SYSTEM\CurrentControlSet\Services\UsbStor\Start"

Set objShell = CreateObject("WScript.Shell")

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "ReadWPUSBStor", ""
   Err.Clear
End If

strWriteProtect = objShell.RegRead(strWriteProtect)
strUsbStor = objShell.RegRead(strUsbStor)

Err.Clear

if strWriteProtect = "1" Then funcresult = funcresult + 2
if strUsbStor = "4" Then funcresult = funcresult + 3

Set objWMI = GetObject("winmgmts://" & "." & "/root\cimv2")
Set objInstances = objWMI.InstancesOf("Win32_USBController")

if objInstances.Count = "0" Then
   funcresult = 5
End if

Select Case funcresult
       Case "0"       ReadWPUSBStor = "enabled"
       Case "2"       ReadWPUSBStor = "read-only"
       Case "3"       ReadWPUSBStor = "disabled"
       Case "5"       ReadWPUSBStor = "disabled"
End Select

If ReadWPUSBStor = "" Then ReadWPUSBStor = "unknown"

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "ReadWPUSBStor", ""
   Err.Clear
End If
On Error GoTo 0
End Function
'======================================

''''''''''''''''''''''''''''''''''''''
Function CheckMember(sLogName)
''''''''''''''''''''''''''''''''''''''
On Error Resume Next
Dim strResultf
Dim objWshNetwork
Dim strComputer, strGroupSID, strGroupName, objGroup, objMember
strResultf = false
sLogName = Mid(sLogName, InStr(sLogName, "\") + 1, Len(sLogName))
strGroupSID = "S-1-5-32-547"  ' Well Known SID of the Administrators group
' Obtain the group name based on well know SID
strGroupName = GetGroupName(".", strGroupSID)
' Connect to the group
Set objGroup = GetObject("WinNT://" & "." & "/" & strGroupName & ",group")
' Display all member names in the group
For Each objMember in objGroup.Members
    if StrComp(objMember.Name,sLogName) = 0 Then
        strResultf = true
    End if
Next
CheckMember = strResultf

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "CheckMember", ""
   Err.Clear
End If
On Error GoTo 0
End Function
'======================================

''''''''''''''''''''''''''''''''''''''
Function GetGroupName(sComputer, sGroupSID)
''''''''''''''''''''''''''''''''''''''
On Error Resume Next
' List of well know SID's is available here:
'
' Well-known security identifiers in Windows operating systems
' http://support.microsoft.com/?id=243330
Dim oGroupAccounts, oGroupAccount
Set oGroupAccounts = GetObject("winmgmts://" & sComputer & "/root/cimv2").ExecQuery("Select Name from Win32_Group" & " WHERE Domain = '" & sComputer & "' AND SID = '" & sGroupSID & "'")
If oGroupAccounts.Count = 0 Then
' need to use Domain = 'BUILTIN' at least for Win2k SP2
Set oGroupAccounts = GetObject("winmgmts://" & sComputer & "/root/cimv2").ExecQuery("Select Name from Win32_Group"& " WHERE Domain = 'BUILTIN' AND SID = '" & sGroupSID & "'")
End If
For Each oGroupAccount In oGroupAccounts
GetGroupName = oGroupAccount.Name
Next
If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetGroupName", ""
   Err.Clear
End If
On Error GoTo 0
End Function
'======================================

'======================================


Function CheckIsAdmin
On Error Resume Next
Dim strResultf
Dim listItem
	
strResultf = "N"

if InStr(1,strLocalPUGroupMembers, Mid(strLoginName, InStr(strLoginName,"\") + 1, Len(strLoginName)),1) <> 0 Then
strResultf = "P"
End if

If InStr(1,strLocalAdminGroupMembers, Mid(strLoginName, InStr(strLoginName,"\") + 1, Len(strLoginName)),1) <> 0 then
strResultf = "Y"
End If

for each listItem in groupListD
If InStr(1,strLocalAdminGroupMembers, Mid(listItem, InStr(listItem,"\") + 1, Len(listItem)),1) <> 0 then
strResultf = "Y"
End If
next

CheckIsAdmin = strResultf

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "CheckIsAdmin", ""
   Err.Clear
End If
On Error GoTo 0
End Function
'======================================
'======================================
'symantec AV DB Date
Function GetAVDate
On Error Resume Next
Dim objWMIService
Dim colLoggedEvents
Dim colItemLoggedEvents

GetAVDate="1900-01-01"

Set objWMIService = GetObject("winmgmts:" _
& "{impersonationLevel=impersonate}!\\" & "." & "\root\cimv2")
Set colLoggedEvents = objWMIService.ExecQuery _
("Select * from Win32_NTLogEvent Where Logfile = 'Application' and SourceName='Symantec AntiVirus' and EventCode='7'")'

For each colItemLoggedEvents in colLoggedEvents
    GetAVDate = Left(colItemLoggedEvents.TimeWritten,4)&"-"&Mid(colItemLoggedEvents.TimeWritten,5,2)&"-"&Mid(colItemLoggedEvents.TimeWritten,7,2)
    Exit For
Next

'=====begin kaspersky part
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & "." & "\root\cimv2")
Set colLoggedEvents = objWMIService.ExecQuery("Select * from Win32_Service where DisplayName like '%kaspersky%'")
if colLoggedEvents.count <> 0 Then
    GetAVDate = "1999-12-12"
For Each colItemLoggedEvents in colLoggedEvents
    If InStr(colItemLoggedEvents.DisplayName,"Anti-Virus") <> 0 and colItemLoggedEvents.State ="Stopped" Then
        GetAVDate = Replace(GetAVDate,"-12-","-03-")
    ElseIf InStr(colItemLoggedEvents.DisplayName,"Anti-Virus") <> 0 and colItemLoggedEvents.State ="Running" Then
        GetAVDate = Replace(GetAVDate,"-12-","-04-")
    End if
    If InStr(colItemLoggedEvents.DisplayName,"ky Lab Net") <> 0 and colItemLoggedEvents.State ="Stopped" Then
        GetAVDate = Replace(GetAVDate,"-12","-07")
    ElseIf InStr(colItemLoggedEvents.DisplayName,"ky Lab Net") <> 0 and colItemLoggedEvents.State ="Running" Then
        GetAVDate = Replace(GetAVDate,"-12","-08")
    End if
Next
End if
'====== end kaspersky part

If Err.Number <> 0 Then
   WriteToErrorLog Err.Number, Err.Description, "GetAVDate", ""
   Err.Clear
End If
On Error GoTo 0
End Function
'======================================


Function WriteToErrorLog (intErrNumber, strErrDescription, strFunctionName, strTypeError)
On Error Resume Next
Dim objNet
Dim objConn, objRecordset
Dim ServerName, DSN, UID, PWD, ConnectString, ExecuteString, strScriptName
strScriptName="GetInfo.vbs"
Dim ErrorType
If strTypeError="" Then
	ErrorType = "GETINFO"
Else
	ErrorType=strTypeError
End If

Set objNet = CreateObject("WScript.NetWork")
Set objConn = CreateObject("ADODB.Connection")
'==================
	ServerName = "srvsql01.rs.ru" 	'��� ��� IP-����� �������
	DSN = "" 			'��� ���� ������
	UID = "" 		'����� ������������ SQL-�������
	PWD = "" 			'������ ������������ SQL-�������
'==================
	ConnectString = "Provider=SQLOLEDB;"&"Data Source="&ServerName&";Initial Catalog="&DSN&";UID="&UID&";PWD="&PWD
	objConn.ConnectionString = ConnectString
	objConn.ConnectionTimeOut = 15
	objConn.CommandTimeout = 30
	objConn.Open

	Dim strTime,strComputeNameErrLog, strErrNumber
	strTime=Year(Date)&"-"&Month(Date)&"-"&Day(Date)&" "&Hour(Now)&":"&Minute(Now)&":"&Second(Now)
	strComputeNameErrLog=objNet.ComputerName
	strErrNumber= "0x"&(Hex(intErrNumber)) 
	
	ExecuteString="EXEC SaveErrorToErrorLog '"&strTime&"', '"&strComputeNameErrLog&"', '"&strErrNumber&"', '"&strErrDescription&"', '"&ErrorType&"', '"&strScriptName&"', '"&strFunctionName&"'"
	Set objRecordset = objConn.Execute(ExecuteString)
	objConn.Close
	Set objConn = Nothing
	Set objRecordset = Nothing
On Error GoTo 0
End Function
'======================================


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function SaveToSQLDataBase															'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 
Function SaveLogToSQLDataBase (strComputerName, strComputerIP, strComputerIPType, strComputerLocalAdmins, strComputerLocalPU, strComputerDate, strLoginName, strPassChangeDate, strOSName, strUSBStatus, strIsAdmin, strAVData)
On Error Resume Next
	Dim objConn, objRecordset
	Dim ServerName, DSN, UID, PWD, ConnectString, ExecuteString
	
	'������ ��� �����������
	Set objConn = CreateObject("ADODB.Connection")
	
	'���������� ��������� ����������� � ���� ������
	ServerName = "" 		'��� ��� IP-����� �������
	DSN = "" 			'��� ���� ������
	UID = "" 			
	PWD = "" 
	
	ConnectString = "Provider=SQLOLEDB;"&"Data Source="&ServerName&";Initial Catalog="&DSN&";UID="&UID&";PWD="&PWD
	objConn.ConnectionString = ConnectString
	objConn.ConnectionTimeOut = 15
	objConn.CommandTimeout = 30
	
	'������������ � ���� ������
	objConn.Open
	
	'��������� ������
	ExecuteString="EXEC SaveUserLogsData2 '"&strComputerName&"', '"&strComputerIP&"', '"&strComputerIPType&"', '"&strComputerLocalAdmins&"', '"&strComputerLocalPU&"', '"&strComputerDate&"', '"&strLoginName&"', '"&strOSName&"', '"&strUSBStatus&"', '"&strIsAdmin&"', '"&strAVData&"'"
											
	'��������� ������
	Set objRecordset = objConn.Execute(ExecuteString)
	
	'��������� ����������
	objConn.Close
	Set objConn = Nothing
	Set objRecordset = Nothing
	

' If Err.Number <> 0 Then
'    WriteToErrorLog Err.Number, Err.Description, "SaveToSQLDataBase", ""
'    Err.Clear
' End If
On Error Goto 0

End Function
