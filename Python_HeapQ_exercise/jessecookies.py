#!/usr/bin/python3

import os
import sys
#import heapq

#
# Complete the cookies function below.
#
def cookies(k, A):
    #
    # Write your code here.
    #
    #A.sort()
    #print(A)
    n = len(A)
    buildheap(A, n)
    #print('after the heap is built:', A)


    ntries = 0
    
    while A[0] < k and len(A)>1:
        ntries += 1
        #incsweet(A)
        incsweet2020(A)
    
    if len(A) > 1:
        #print('after:',A)
        return ntries
    else:
        if A[0] >= k:
         #   print('after:',A)
            return ntries
        else:
          #  print('after:',A)
            return -1
    
def cookies2020(k, A):
    n = len(A)
    buildheap(A, n)
    #print('after the heap is built:', A)


    ntries = 0
    
    while A[0] < k and ntries < 1000000:
        m1 = del_1cookie(A)
        if A==[]:
            return -1
        m2 = del_1cookie(A)
        add_newcookieBU(A, m1+2*m2)
        ntries += 1
    return ntries    



def incsweet2020(A):
    m1 = del_1cookie(A)
    m2 = del_1cookie(A)
    add_newcookieBU(A, m1+2*m2)


def incsweet(A):
    #newcookie = A[0]+2*A[1]
    if len(A) > 2 and A[2] < A[1]:
        newcookie = A[0]+2*A[2]
        add_newcookie(A,newcookie)
        del_2cookie(A,2)
#        A.pop(0)
#        A.pop(1)
        n = len(A)
        heapify(A, n, 2)
    else:
        newcookie = A[0]+2*A[1]
        add_newcookie(A,newcookie)
        del_2cookie(A,1)
#        A.pop(0)
#        A.pop(0)        
        n = len(A)
        heapify(A, n, 1)

    n = len(A)
    heapify(A, n, 0)


    #reheapifyBU(A, n, n-1)
    #A.sort()
 #   print('after a sort:', A)

def add_newcookie(A, nc):
    #print('add cookie:', nc)
    A.append(nc)
  #  print('after append:',A)

def del_1cookie(A):
    m = A[0]
    A[0] = A[-1]
    A.pop()
    n = len(A)
    heapify(A, n, 0)
    return m

def add_newcookieBU(A, nc):
    A.append(nc)
    n=len(A)
    reheapifyBU(A,n,n-1)

def del_2cookie(A,i):
   # print('del_cookie before:',A)
    A[0], A[i] = A[-1], A[-2]
    A.pop()
    A.pop()
    #print('del_cookie after:',A)    

#def reheapify(A): --- don't work correctly
    #print('reheap before:', A)
    #n = len(A) - 1


    #for i in range(n,-1,-1):
        #parent = (i - 1) // 2
        #if (parent > -1):
            #if A[i] < A[parent]:
                #A[i], A[parent] = A[parent], A[i]
        #print(i,parent)

    #if len(A)> 2 and A[1] > A[2]:
        #A[1], A[2] = A[2], A[1]
    #print('reheap after:', A)


def reheapifyBU(A, n, i):
    #print('reheap before:', i, ' ' , A)

    parent = (i - 1) // 2
    
    if (parent > -1):
        if A[i] < A[parent]:
            A[i], A[parent] = A[parent], A[i]
            reheapifyBU(A, n, parent)

    #print('reheap after:', i, '  ', A)


def heapify(A,n,i):
    minh = i
       #print('the initial minh: ', minh)
    l = 2*i + 1
    r = 2*i + 2
 
    if l < n and A[l] < A[i]:
        minh = l
 
    if r < n and A[minh] > A[r]:
        minh = r
 
    if minh != i:
        A[i],A[minh] = A[minh],A[i]
        #   print('A recursive heapify. Heap minh: ',minh)
        heapify(A, n,minh)

def buildheap(A, n):
    print('buildheap before:', A)
    idx = int((n/2)) - 1
    for i in range(idx, -1, -1):
        heapify(A, n, i)
    print('buildheap after:', A)



    
        

    
        

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    A = list(map(int, input().rstrip().split()))

    #result = cookies(k, A)
    result = cookies2020(k, A)


    fptr.write(str(result) + '\n')

    fptr.close()

